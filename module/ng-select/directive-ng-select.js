define(function () {
  var directiveNgSelect = function () {
    return {
      restrict: 'E',
      templateUrl: 'module/ng-select/tmpl-ng-select.html',
      scope: {
        options: '=',
        selected: '=',
        callback: '&'
      },
      controller: function ($scope) {
        console.log('ng-select loaded');

        if (typeof $scope.options === 'undefined') {
          throw 'ngSelect: options attribute is required.'
        }

        // Turn array of strings to array of option objects.
        if (typeof $scope.options[0] !== 'object') {
          $scope.optionList = $scope.options.map(function (option) {
            return {
              label: option,
              value: option
            }
          });
        }

        // Find the selected option obj
        $scope.optionList.forEach(function (option) {
          if (option.value === $scope.selected) {
            $scope.selectedObj = option;
          }
        });

        $scope.updateSelected = function () {
          $scope.selected = $scope.selectedObj.value;

          setTimeout(function () {
            $scope.callback();
          });
        };
      }
    }
  };

  return directiveNgSelect;
});