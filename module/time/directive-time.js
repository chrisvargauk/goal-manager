define(function () {
  var directiveTime = function() {
    return {
      restrict: 'E',
      scope: {
        modelHour: '=',
        modelMin: '=',
        callback: '&'
      },
      templateUrl: 'module/time/tmpl-time.html',
      controller: function ($scope) {
        console.log('Time loaded', $scope.modelHour);

        // Check whether all necessary attrs are set
        if (typeof $scope.modelHour === 'undefined') {
          console.error('time: modelHour attribute is not set.');
        }

        if (typeof $scope.modelMin === 'undefined') {
          console.error('time: modelMin attribute is not set.');
        }

        if (typeof $scope.callback === 'undefined') {
          console.error('time: callback attribute is not set.');
        }

        $scope.listHour = createOptionList(0, 24);
        $scope.hourSelected = $scope.listHour[$scope.modelHour];

        $scope.callCallback = function () {
          $scope.modelHour = $scope.hourSelected.value;
          $scope.modelMin = $scope.minSelected.value;

          if (typeof $scope.callback !== 'undefined') {
            setTimeout(function () {
              $scope.callback();
            }, 0);
          }
        };

        $scope.listMinute = createOptionList(0, 60);
        $scope.minSelected = $scope.listMinute[$scope.modelMin];

        function createOptionList(ctrStart, ctrStop) {
          var list = [];
          for (var i = ctrStart; i < ctrStop; i++) {
            list.push(i);
          }

          return list.map(function (digit) {
            var label;
            if (digit < 10) {
              label = '0' + digit;
            } else {
              label = digit + '';
            }

            return {
              label: label,
              value: digit
            }
          });
        }
      }
    };
  };

  return directiveTime;
});