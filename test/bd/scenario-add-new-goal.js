define(['scenarioRunner'], function() {
//  scRunner.add('Create multi-dim collection', function (sc) {
//
//  });

  return function () {
    location.hash = '/home';

    scRunner.log('# At Start: Go to Home page and wait till its loaded');

    location.hash = "#/home";
    scRunner.waitTill('Home page is loaded',
      function () {
        return  document.querySelector('.navigation-bar__title.ng-binding') &&
                document.querySelector('.navigation-bar__title.ng-binding').textContent;
      },
      "TODO it Right ",
      clickSetNewGoalButton
    );

//    scRunner.doItAndWaitTill('At Start: Go to Home page and wait till its loaded',
//      function () {
//        location.hash = "#/home";
//      },
//      'Home page is loaded',
//      function () {
//        return  document.querySelector('.navigation-bar__title.ng-binding') &&
//                document.querySelector('.navigation-bar__title.ng-binding').textContent;
//      },
//      "TODO it Right "
//    );

    function clickSetNewGoalButton () {
      scRunner.log('# On Home page: Click Set New Goal button and wait till page is loaded');

      document.querySelector('footer .button--large--cta').click();

      scRunner.waitTill('Set New Goal page is loaded',
        function () {
          return  document.querySelector('.navigation-bar__title.ng-binding') &&
                  document.querySelector('.navigation-bar__title.ng-binding').textContent;
        },
        "Set New Goal ",
        clickStartButtonAndWaitForPageLoad
      );
    }

    function clickStartButtonAndWaitForPageLoad() {
      scRunner.log('# On Introduction page: Click Start Button and wait till page is loaded');

      document.querySelector('.button--large--cta').click();

      scRunner.waitTill('Definition page is loaded',
        function () {
          return document.querySelector('.navigation-bar__title.ng-binding').textContent;
        },
        "Definition ",
        fillInInput);
    }

    function fillInInput() {
      scRunner.log('# On Definition page: fill in input fields, click next and wait till page is loaded');

      document.querySelector('input').focus();
      document.querySelector('input').value = "Test Goal";
      angular.element(document.querySelector('input')).trigger('input');
      document.querySelector('input').blur();
      document.querySelector('textarea').focus();
      document.querySelector('textarea').value = "Test Description";
      angular.element(document.querySelector('textarea')).trigger('input');
      document.querySelector('textarea').blur();
      document.querySelector('input').focus();

      document.querySelector('.pure-button-primary').click();

      scRunner.waitTill('Steps page is loaded',
        function () {
          return document.querySelectorAll('row').length;
        },
        1,
        fillInDefaultStep
      );
    }

    function fillInDefaultStep() {
      scRunner.log('# On Steps page: fill in input filed for default step than click finish');

      document.querySelector('textarea').value = "Test Step";
      angular.element(document.querySelector('textarea')).trigger('input');
      document.querySelector('textarea').focus();
      document.querySelector('textarea').blur();

      clickAddMoreSteps();
    }

    function clickAddMoreSteps() {
      scRunner.log('# On Steps page: click Add More Steps and wait till its rendered');

      document.querySelector('.button--large--cta').click();

      scRunner.waitTill('New Step is Rendered',
        function () {
          return document.querySelectorAll('row').length;
        },
        2,
        deleteNewStep
      );
    }

    function deleteNewStep() {
      scRunner.log('# On Steps page: Delete New Step and wait till its rendered');

      document.querySelectorAll('.icon-bin')[1].click()

      scRunner.waitTill('New Step is Rendered',
        function () {
          return document.querySelectorAll('row').length;
        },
        1,
        clickFinish
      );
    }

    function clickFinish() {
      scRunner.log('# On Steps page: Click finish and wait till home page is loaded');

      document.querySelector('.pure-button-primary').click();

      scRunner.waitTill('Home page is loaded',
        function () {
          return  document.querySelector('.navigation-bar__title.ng-binding') &&
            document.querySelector('.navigation-bar__title.ng-binding').textContent;
        },
        "TODO it Right ",
        checkNewListItem
      );
    }

    function checkNewListItem() {
      scRunner.add('On Home page: Check properties of new list item representing the new goal', function (sc) {
        sc.test('Check Goal Title')
          .check(document.querySelectorAll('.plan-name')[0].textContent)
          .equalTo("Test Goal");

        sc.test('Check New Goal Description')
          .check(document.querySelectorAll('.description')[0].textContent.trim())
          .equalTo("Test Description");

        sc.test('Check New Goal Next Step')
          .check(document.querySelectorAll('.next-step')[0].textContent.trim())
          .equalTo("Test Step");

        sc.test('Check New Goal Progress')
          .check(document.querySelectorAll('.progress')[0].textContent.trim())
          .equalTo("Progress: 0%");
      });

      scRunner.run('all');
    }
  }
});