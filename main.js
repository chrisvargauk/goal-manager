require.config({
  paths: {
    'ng': 'lib/vendor/angular',
    'underscore': 'lib/vendor/underscore',
    'uiRouter': 'lib/vendor/angular-ui-router',
    'moment': 'lib/vendor/moment',
    'websql': "lib/websql",
    'Collection': 'lib/angularjs-collection',
    'onsen': "lib/vendor/onsen/js/onsenui",
    'pageManager': 'module/sliding-menu/page-manager',
    'CtrlApp': 'ctrl-app',
    'directiveAlert': 'module/alert/directive-alert',
    'directiveTime': 'module/time/directive-time',
    'directiveNgSelect': 'module/ng-select/directive-ng-select',
    'directiveProgressbar': 'product/progressbar/directive-progressbar',
    'directiveSchedule': 'product/page/home/directive-schedule',
    'directiveTrackTime': 'product/page/goal-detail/directive-track-time',
    'deferredListCollection': 'product/page/add-new-goal/partial/deferred-list-collection',

    'CtrlHome': 'product/page/home/CtrlHome',
    'CtrlIntroduction': 'product/page/add-new-goal/partial/CtrlIntroduction',
    'CtrlListReason': 'product/page/add-new-goal/partial/CtrlListReason',
    'CtrlDefinition': 'product/page/add-new-goal/partial/CtrlDefinition',
    'CtrlListStep': 'product/page/add-new-goal/partial/CtrlListStep',
    'CtrlListReward': 'product/page/add-new-goal/partial/CtrlListReward',
    'CtrlListTimeframe': 'product/page/add-new-goal/partial/CtrlListTimeframe',
    'CtrlGoalDetail': 'product/page/goal-detail/CtrlGoalDetail',
    'CtrlTrackRecord': 'product/page/track-record/CtrlTrackRecord',
    'CtrlReview': 'product/page/review/CtrlReview',
    'CtrlSetting': 'product/page/setting/CtrlSetting',
    'CtrlAbout': 'product/page/about/CtrlAbout',

    'resolveReward': 'product/page/add-new-goal/partial/resolveReward',

    'scenarioRunner': 'lib/scenarioRunner',
    'testRunnerScenario': 'test/bd/test-runner-scenario',
    'testRunnerUnit': 'test/unit-test/test-runner-unit',
    'CtrlTestRunner': 'test/bd/CtrlTestRunner'
  }
});

define(['app',
        'testRunnerScenario',
        'testRunnerUnit'
  ], function () {
    'use strict';

    angular.bootstrap(document, ['app']);
});