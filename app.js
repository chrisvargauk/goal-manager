define(['config',
        'websql',
        'Collection',
        'onsen',
        'CtrlApp',
        'pageManager',
        'directiveAlert',
        'directiveTime',
        'directiveNgSelect',
        'directiveProgressbar',
        'directiveSchedule',
        'directiveTrackTime',

        'CtrlHome',
        'CtrlIntroduction',
        'CtrlListReason',
        'CtrlDefinition',
        'CtrlListStep',
        'CtrlListReward',
        'CtrlListTimeframe',
        'CtrlGoalDetail',
        'CtrlTrackRecord',
        'CtrlReview',
        'CtrlSetting',
        'CtrlAbout',
        'resolveReward',

        'ng',
        'underscore',
        'moment',
        'uiRouter',
        'deferredListCollection',
        'scenarioRunner'],
  function(config,
           websql,
           Collection,
           onsen,
           CtrlApp,
           pageManager,
           directiveAlert,
           directiveTime,
           directiveNgSelect,
           directiveProgressbar,
           directiveSchedule,
           directiveTrackTime,

           CtrlHome,
           CtrlIntroduction,
           CtrlListReason,
           CtrlDefinition,
           CtrlListStep,
           CtrlListReward,
           CtrlListTimeframe,
           CtrlGoalDetail,
           CtrlTrackRecord,
           CtrlReview,
           CtrlSetting,
           CtrlAbout,
           resolveReward,

           ng,
           underscore,
           momentjs,
           uiRouter,
           deferredListCollection,
           scenarioRunner) {

    app = angular.module('app', ['onsen', 'ui.router']);

    app.factory('sharedDataAddNewGoal', function () {
      return {};
    });

    app.directive('elastic', [
      '$timeout',
      function($timeout) {
        return {
          restrict: 'A',
          link: function($scope, element) {
            var resize = function() {
              return element[0].style.height = "" + element[0].scrollHeight + "px";
            };
            element.on("blur keyup change", resize);
            $timeout(resize, 0);
          }
        };
      }
    ]);

    app.run(function ($rootScope, $log) {
      $rootScope.$log = $log;
    });

    app.controller('CtrlApp', CtrlApp);
    app.factory('pageManager', pageManager);
    app.directive('alert', directiveAlert);
    app.directive('time', directiveTime);
    app.directive('ngSelect', directiveNgSelect);
    app.directive('progressbar', directiveProgressbar);
    app.directive('schedule', directiveSchedule);

    app.directive('trackTime', directiveTrackTime);


    app.config(config);
  }
);