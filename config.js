define(['deferredListCollection'], function () {

  function config($stateProvider, $urlRouterProvider) {
    // # Redirects
    $urlRouterProvider
      .when('', '/home')
      .when('/add-new-goal', 'add-new-goal/introduction')
      .otherwise('/home');

    // # Pages
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'product/page/home/home-tmpl.html',
        controller: CtrlHome,
        resolve: {
          cReason: deferredFnReason,
          cStep: deferredFnStep,
          cReview: deferredFnReview,
          cUserGoal: deferredFnUserGoal
        }
      })

      .state('add-new-goal', {
        url: '/add-new-goal',
        templateUrl: 'product/page/add-new-goal/tmpl-add-new-goal.html'
      })
      .state('add-new-goal.introduction', {
        url: '/introduction',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-introduction.html',
        controller: CtrlIntroduction,
        resolve: {
          cReason: deferredFnReason,
          cStep: deferredFnStep,
          cUserGoal: deferredFnUserGoal,
          cTimeAndLocation: deferredFnTimeAndLocation,
          cTrackRecord: deferredFnTrackRecord
        }
      })
      .state('add-new-goal.definition', {
        url: '/definition',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-definition.html',
        controller: CtrlDefinition
      })
      .state('add-new-goal.ListReason', {
        url: '/reasons',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-reason.html',
        controller: CtrlReason
      })
      .state('add-new-goal.listStep', {
        url: '/steps',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-step.html',
        controller: CtrlListStep
      })
      .state('add-new-goal.listReward', {
        url: '/rewards',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-reward.html',
        controller: CtrlListReward
      })
      .state('add-new-goal.listTimeframe', {
        url: '/timeframe',
        templateUrl: 'product/page/add-new-goal/partial/tmpl-list-timeframe.html',
        controller: CtrlListTimeframe,
        resolve: {
          cReason: deferredFnReason,
          cStep: deferredFnStep,
          cUserGoal: deferredFnUserGoal,
          cTimeAndLocation: deferredFnTimeAndLocation,
          cTrackRecord: deferredFnTrackRecord
        }
      })

      .state('goal-detail', {
        url: '/goal-details/:indexUserGoal',
        templateUrl: 'product/page/goal-detail/tmpl-goal-detail.html',
        controller: CtrlGoalDetail,
        resolve: {
          cReason: deferredFnReason,
          cStep: deferredFnStep,
          cUserGoal: deferredFnUserGoal
        }
      })

      .state('track-record', {
        url: '/track-record/:indexUserGoal',
        templateUrl: 'product/page/track-record/tmpl-track-record.html',
        controller: CtrlTrackRecord,
        resolve: {
          cUserGoal: deferredFnUserGoal
        }
      })

      .state('review', {
        url: '/review/:idUserGoal/:idReview',
        templateUrl: 'product/page/review/tmpl-review.html',
        controller: CtrlReview,
        resolve: {
          cUserGoal: deferredFnUserGoal
        }
      })

      .state('setting', {
        url: '/setting',
        templateUrl: 'product/page/setting/tmpl-setting.html',
        controller: CtrlSetting
      })

      .state('about', {
        url: '/about',
        templateUrl: 'product/page/about/tmpl-about.html',
        controller: CtrlAbout
      });
  }

  config.$inject=['$stateProvider', '$urlRouterProvider'];

  return config;
});