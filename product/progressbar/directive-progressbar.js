define(function () {
  var directiveProgressbar = function () {
    return {
      restriction: 'E',
      templateUrl: 'product/progressbar/progressbar-tmpl.html',
      scope: {
        mUserGoal: '='
      },
      controller: function ($scope) {
        $scope.noStepDone = 0;
        $scope.mUserGoal.cStep.JSON.forEach(function (mStep) {
          if (mStep.isDone) {
            $scope.noStepDone++;
          }
        });

        $scope.getBackgroundColor = function (index) {
          if (index < $scope.noStepDone) {
            return '#3399ff';
          } else {
            return 'none';
          }
        };

        var cStepLength = $scope.mUserGoal.cStep.JSON.length;
        if (cStepLength) {
          $scope.barHeight = (100 / $scope.mUserGoal.cStep.JSON.length) + '%';
        } else {
          $scope.barHeight = 0;
        }

      }
    }
  };

  return directiveProgressbar;
});
