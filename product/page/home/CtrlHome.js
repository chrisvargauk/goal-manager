var CtrlHome = function ($scope, cUserGoal, $rootScope, pageManager) {
  pageManager.setTitle('TODO it Right');

  $scope.model = {};
  $scope.model.cUserGoal = cUserGoal;

  //todo: delete for prod.
  window.cUserGoal = cUserGoal;

  this.goToUserGoalDetail = function (indexUserGoal) {
    location.hash = '#/goal-details/'+indexUserGoal;
  };

  $scope.removeUserGoal = function (id) {
    $rootScope.$emit('alert', {
      title: 'DELETE',
      text: 'Do you really want to delete this goal?',
      type: 'cancelOk',
      fnOk: function () {
        $scope.model.cUserGoal.removeById('userGoal', id, function() {
          $scope.$apply(function () {});
        });
      }
    });
  };

  $scope.getProgress = function (mUserGoal) {
    var noStepDone = 0;
    mUserGoal.cStep.JSON.forEach(function (mStep) {
      if (mStep.isDone) {
        noStepDone++;
      }
    });

    if (mUserGoal.cStep.JSON.length) {
      return Math.round(noStepDone / (mUserGoal.cStep.JSON.length / 100)) + '%';
    } else {
      return '0%';
    }
  };

  $scope.getNextStep = function (cStep) {
    var mStepNext;

    for (index in cStep.JSON) {
      var mStep = cStep.JSON[index];

      if (!mStep.isDone) {
        mStepNext = mStep;
        break;
      }

    }

    return mStepNext;
  };

  $scope.goToUserGoalDetail = function (indexUserGoal) {
    location.hash = '#/goal-details/'+indexUserGoal;
  };
};