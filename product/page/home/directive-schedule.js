define(function () {
  var directiveSchedule = function () {
    return {
      restrict: 'E',
      templateUrl: 'product/page/home/tmpl-schedule.html',
      scope: {
        mUserGoal: '='
      },
      controller: function ($scope) {
        $scope.listDay = [
          {
            label: 'Mo',
            value: 'Monday'
          },
          {
            label: 'Tu',
            value: 'Tuesday'
          },
          {
            label: 'We',
            value: 'Wednesday'
          },
          {
            label: 'Th',
            value: 'Thursday'
          },
          {
            label: 'Fr',
            value: 'Friday'
          },
          {
            label: 'Sa',
            value: 'Saturday'
          },
          {
            label: 'Su',
            value: 'Sunday'
          }
        ];

        $scope.listDaySimple = _.pluck($scope.listDay, 'value');

        $scope.listDayAccomplished = makeListDayAccomplished();

        $scope.getDayStyle = function (dayBreaf) {
          var listClass = [];

          if (isToday(dayBreaf.value)) {
            listClass.push('today');
          }

          if (isAccomplished(dayBreaf.value)) {
            listClass.push('accomplished');
          }

          if (isScheduled(dayBreaf.value)) {
            listClass.push('scheduled');

            if (isFailed(dayBreaf.value)) {
              listClass.push('failed');
            }
          }

          return listClass;
        };

        function makeListDayAccomplished() {
          listDayAccomplished = [];

          $scope.mUserGoal.cTrackRecord.JSON.forEach(function (record) {
            if (record.weekOfYear == moment().week() &&
              listDayAccomplished.indexOf(record.dayOfWeek) === -1
              ) {
              listDayAccomplished.push(record.dayOfWeek);
            }
          });

          return listDayAccomplished;
        }

        function isScheduled(dayOfWeek) {
          return _.pluck($scope.mUserGoal.cTimeAndLocation.JSON, 'day').indexOf(dayOfWeek) !== -1;
        }

        function isAccomplished(dayOfWeek) {
          return $scope.listDayAccomplished.indexOf(dayOfWeek) !== -1;
        }

        function isFailed(dayOfWeek) {
          var numberDayOfWeekIter = $scope.listDaySimple.indexOf(dayOfWeek),
            numberDayOfWeekCurrent = moment().day() - 1;

          return  numberDayOfWeekIter < numberDayOfWeekCurrent && !isAccomplished(dayOfWeek);
        }

        function isToday(dayOfWeek) {
          var numberDayOfWeekIter = $scope.listDaySimple.indexOf(dayOfWeek),
            numberDayOfWeekCurrent = moment().day() - 1;

          return  numberDayOfWeekIter === numberDayOfWeekCurrent;
        }
      }
    };
  }

    return directiveSchedule;
});