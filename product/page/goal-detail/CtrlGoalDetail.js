var CtrlGoalDetail = function ($scope, $stateParams, cUserGoal, sharedDataAddNewGoal, pageManager, $rootScope) {
  console.log('Ctrl Goal Details is loaded');

  pageManager.setTitle('Goal Details');

  $scope.model = {
    done: false,
    inProgress: false
  };
  $scope.model.cUserGoal = cUserGoal;
  $scope.model.mUserGoal = cUserGoal.getById($stateParams.indexUserGoal);

  $scope.listLabel = {
    startStop: 'Start'
  };

  $scope.listDaySimple = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
  ];

  $scope.hideTip = function () {
    $scope.model.mUserGoal.tipVisible = false;
    $scope.model.cUserGoal.check();
  };

  $scope.listDayAccomplished = makeListDayAccomplished();

  $scope.editUserGoal = function () {
    sharedDataAddNewGoal.cUserGoal = $scope.model.cUserGoal;
    sharedDataAddNewGoal.mUserGoal = $scope.model.mUserGoal;
    location.hash = '#/add-new-goal/definition';
  };

  $scope.goToRewards = function () {
    sharedDataAddNewGoal.cUserGoal = $scope.model.cUserGoal;
    sharedDataAddNewGoal.mUserGoal = $scope.model.mUserGoal;
    location.hash = '#/add-new-goal/rewards';
  };

  $scope.goToReasons = function () {
    sharedDataAddNewGoal.cUserGoal = $scope.model.cUserGoal;
    sharedDataAddNewGoal.mUserGoal = $scope.model.mUserGoal;
    location.hash = '#/add-new-goal/reasons';
  };

  $scope.goToListTimeframe = function () {
    sharedDataAddNewGoal.cUserGoal = $scope.model.cUserGoal;
    sharedDataAddNewGoal.mUserGoal = $scope.model.mUserGoal;
    location.hash = '#/add-new-goal/timeframe';
  };

  $scope.goToTrackRecord = function (idUserGoal) {
    location.hash = '#/track-record/' + idUserGoal;
  };

  $scope.showGoToRewards = function () {
    // If there is a single reward set dont show button, otherwise show it.
    for (index in $scope.model.mUserGoal.cStep.JSON) {
      var mStep = $scope.model.mUserGoal.cStep.JSON[index];

      if (mStep.reward !== '') {
        return false;
      }
    }

    return true;
  };

  $scope.showCongrat = function (step) {
    if (!step.isDone || !step.reward) {
      return;
    }

    $rootScope.$emit('alert', {
      title: 'Good Job',
      text: 'Now reward yourself: ' + step.reward,
      fnOk: function () {
      }
    });
  };

  $scope.addZero = function (digit) {
    if (digit < 10) {
      return '0'+digit;
    } else {
      return digit;
    }
  };

  $scope.getDayStyle = function (day) {
    var listClass = [];

    if ( isToday(day) ) {
      listClass.push('today');
    }

    if ( isAccomplished(day) ) {
      listClass.push('accomplished');
    }

    if ( isFailed(day) ) {
        listClass.push('failed');
    }

    return listClass;
  };

  function isToday(dayOfWeek) {
    var numberDayOfWeekIter = $scope.listDaySimple.indexOf(dayOfWeek),
        numberDayOfWeekCurrent = getIsoDay();

    return  numberDayOfWeekIter === numberDayOfWeekCurrent;
  }

  function isAccomplished(dayOfWeek) {
    return $scope.listDayAccomplished.indexOf(dayOfWeek) !== -1;
  }

  function makeListDayAccomplished() {
    listDayAccomplished = [];

    $scope.model.mUserGoal.cTrackRecord.JSON.forEach(function (record) {
      if (record.weekOfYear ==  dateNow().week() && // if current week
          listDayAccomplished.indexOf(record.dayOfWeek) === -1 // if not on the list already
      ) {
        listDayAccomplished.push(record.dayOfWeek);
      }
    });

    return listDayAccomplished;
  }

  function isFailed(dayOfWeek) {
    var numberDayOfWeekIter = $scope.listDaySimple.indexOf(dayOfWeek),
        numberDayOfWeekCurrent = getIsoDay();

    return  numberDayOfWeekIter < numberDayOfWeekCurrent &&
            !isAccomplished(dayOfWeek);
  }

  function dateNow() {
//        return moment().subtract(6, 'days');
//        return moment().subtract(1, 'days');
//        return moment().add(4, 'days');

    return moment();
  }

  function getIsoDay() {
    return (dateNow().day() + 6) % 7;
  }

  // Check if it is Sunday and if it is that whether the review is done or not
  window.isItTimeForReview = function() {
    var isScheduled = false,
        today = moment(),
        mReviewSceduled;

    // Check if it is sunday
    if (moment(today).day() !== 0) {
      var lastSunday = moment(parseInt(today.format('x'))).weekday(0);
      onSunday(lastSunday);
    } else {
      onSunday(today);
    }

    function onSunday(today) {
      // Check whether there is any review scheduled for this Sunday
      $scope.model.mUserGoal.cReview.JSON.forEach(function (mReview) {
//        console.log(mReview.scheduled);
//        console.log(moment(parseInt(mReview.scheduled)).format('WW'));

        if (moment(parseInt(mReview.scheduled)).format('WW') == today.format('WW')) {
//          console.log('scheduled');
          isScheduled = true;
          mReviewSceduled = mReview;
        }
      });

      // if review is scheduled, check whether its done or not
      if(isScheduled) {
        console.log('mReviewSceduled', mReviewSceduled);

        // If review is done than return false
        if (mReviewSceduled.isDone) {
          return false;

          // If scheduled review is not done yet then return true
        } else {
          $rootScope.$emit('alert', {
            title: 'Review',
            text: 'It\'s time to review your week!',
            type: 'cancelOk',
            fnOk: function () {
//              console.log('mReviewSceduled', mReviewSceduled);
              location.hash = '#/review/'+$scope.model.mUserGoal.id+'/'+mReviewSceduled.id;
            }
          });
          return true;
        }

        // if there is no scheduled review then generate one and report true
      } else {
        // if today is earlier date than goal was set, return without generating review
        if (parseInt(today.format('x')) < parseInt($scope.model.mUserGoal.timestamp)) {
          return false;
        }

        generateReview($scope.model.mUserGoal.id, today, function(mReviewSceduled) {
//          console.log('mReviewSceduled', mReviewSceduled);

          $rootScope.$emit('alert', {
            title: 'Review',
            text: 'It\'s time to review your week!',
            type: 'cancelOk',
            fnOk: function () {
//              console.log('mReviewSceduled', mReviewSceduled);
              location.hash = '#/review/'+$scope.model.mUserGoal.id+'/'+mReviewSceduled.id;
            }
          });
        });
        return true;
      }

      return true;
    }
  };

  window.generateReview = function (idUserGoal, today, callback) {
    console.log($scope.model.cUserGoal.getById(idUserGoal).cReview);

    var cReview = $scope.model.cUserGoal.getById(idUserGoal).cReview;

    cReview.add({
      scheduled: today.format('x')
    }, callback);
  };

  isItTimeForReview();
};