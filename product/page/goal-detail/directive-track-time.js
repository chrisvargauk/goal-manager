define(function () {
  var directiveTrackTime = function () {
    return {
      restrict: 'E',
      templateUrl: 'product/page/goal-detail/tmpl-track-time.html',
      scope: {
        cTrackRecord: '='
      },
      controller: function ($scope, $timeout) {
        console.log('trackTime loaded, cTrackRecord', $scope.cTrackRecord);

        var listWeek = [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday'
        ];

        $scope.inProgress = false;
        $scope.buttonLabel = 'Start';

        var size = $scope.cTrackRecord.JSON.length;

        // Select the last model from cTrackRecord
        if (size > 0) {
          $scope.mTrackRecordLast = $scope.cTrackRecord.JSON[(size - 1)];

          // If last tracking is active
          if ($scope.mTrackRecordLast.timeEnd === 'timestamp') {
            $scope.inProgress = true;
            setLabel($scope.inProgress);

            // Set last running tracking as current
            $scope.mTrackRecordCurrent = $scope.mTrackRecordLast;
          }
        }

        $scope.turnOnOff = function () {
          // If there is no running tracking
          if (!$scope.inProgress) {
            var mTrackRecordCurrent = {
              timeStart: dateNow().format('x'),
              timeEnd: 'timestamp',
              weekOfYear: dateNow().week(),
              dayOfWeek: listWeek[(getIsoDay())]
            };

            $scope.cTrackRecord.add(mTrackRecordCurrent, function (model) {
              $scope.mTrackRecordCurrent = model;
            });

          } else {
            $scope.mTrackRecordCurrent.timeEnd = dateNow().format('x');
            $scope.cTrackRecord.check();
          }

          // set inProgress
          $scope.inProgress = !$scope.inProgress;

          setLabel($scope.inProgress);
        };

        $scope.timer = function () {
          console.log('timer');

          if ($scope.inProgress) {
            var timeElapsedSec = Math.round((parseInt(dateNow().format('x')) - parseInt($scope.mTrackRecordCurrent.timeStart)) / 1000),
              timeElapsedMin = Math.floor(timeElapsedSec / 60),
              timeElapsedMinRem = timeElapsedMin % 60,
              timeElapsedSecRem = timeElapsedSec % 60,
              timeElapsedHour = Math.floor(timeElapsedMin / 60);

            //          $scope.timeElapsed = addZero(timeElapsedHour) + ':' + addZero(timeElapsedMinRem) + ':' + (timeElapsedSecRem);
            $scope.timeElapsed = '';

            if (timeElapsedHour) {
              $scope.timeElapsed += addZero(timeElapsedHour) + ':';
            }

            if (timeElapsedMinRem) {
              $scope.timeElapsed += addZero(timeElapsedMinRem) + ':';
            }

            $scope.timeElapsed += addZero(timeElapsedSecRem) + 's';

            $scope.$apply(function () {
            });
          }

          if (location.hash.indexOf('goal-details') !== -1) {
            setTimeout($scope.timer, 1000);
          }
        };

        $scope.timer();

        function setLabel(inProgress) {
          if (inProgress) {
            $scope.buttonLabel = 'Stop';
          } else {
            $scope.buttonLabel = 'Start';
          }
        }

        function dateNow() {
          //        return moment().subtract(6, 'days');
          //        return moment().subtract(1, 'days');
          //        return moment().add(1, 'days');
          return moment();
        }

        function getIsoDay() {
          return (dateNow().day() + 6) % 7;
        }

        function addZero(digit) {
          if (digit < 10) {
            return '0' + digit;
          } else {
            return digit;
          }
        }
      }
    };
  };

  return directiveTrackTime;
});