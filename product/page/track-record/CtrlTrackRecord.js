var CtrlTrackRecord = function ($scope, cUserGoal, $stateParams, sharedDataAddNewGoal, pageManager) {
  console.log('Ctrl Track Record is loaded');

  pageManager.setTitle('History');

  $scope.model = {};
  $scope.model.cUserGoal = cUserGoal;
  $scope.model.mUserGoal = cUserGoal.getById($stateParams.indexUserGoal);
  $scope.model.mUserGoal = cUserGoal.getById($stateParams.indexUserGoal);

  var listTrackRecord = $scope.model.mUserGoal.cTrackRecord.JSON;

  var recordPrev = {},
      firstRecord = moment().format('x');
      weekCurrent = -1;
  $scope.model.listTrackRecordMergeSameDays = [];
  listTrackRecord.forEach(function (record) {
    if (firstRecord > record.timeStart) {
      firstRecord = record.timeStart;
    }

    if (recordPrev.dayOfWeek === record.dayOfWeek &&
        weekCurrent === record.weekOfYear
    ) {
      recordPrev.duration = (parseInt(recordPrev.duration) + (parseInt(record.timeEnd) - parseInt(record.timeStart)))+'';
    } else {
      recordPrev = {
        timeStart: record.timeStart,
        timeEnd: record.timeEnd,
        dayOfWeek: record.dayOfWeek,
        weekOfYear: record.weekOfYear,
        duration: (parseInt(record.timeEnd) - parseInt(record.timeStart))+''
      };

      weekCurrent = record.weekOfYear;
      $scope.model.listTrackRecordMergeSameDays.push(recordPrev);
    }
  });

  var lastModay = moment(parseInt(firstRecord)).startOf('isoweek').day('monday').format('x'),
      today = moment().format('x'),
      ctrMonday = lastModay;

  $scope.model.listWeek = [];
  while (ctrMonday < today) {
    var ctrModayNext = moment(parseInt(ctrMonday)).add(7, 'days').format('x');

    var listTrackRecordCurrentWeek = $scope.model.listTrackRecordMergeSameDays.filter(function (record) {
      return  record.timeStart > ctrMonday &&
              record.timeStart < ctrModayNext;
    });

    listTrackRecordCurrentWeek.reverse();

    var week = {
      start: ctrMonday,
      end: ctrModayNext,
      label: moment(parseInt(ctrMonday)).format('MMMM Do') + ' - ' + moment(parseInt(ctrModayNext)).format('YYYY MMMM Do'),
      listTrackRecordCurrentWeek: listTrackRecordCurrentWeek
    };

    $scope.model.listWeek.push(week);

    ctrMonday = ctrModayNext;
  }
  $scope.model.listWeek.reverse();

  $scope.getDuration = function (duration) {
    if (duration === 'NaN') {
      return 'In Progress';
    }

    var timeElapsedSec = Math.round(duration / 1000),
        timeElapsedMin = Math.floor(timeElapsedSec / 60),
        timeElapsedMinRem = timeElapsedMin % 60,
        timeElapsedSecRem = timeElapsedSec % 60,
        timeElapsedHour = Math.floor(timeElapsedMin / 60),
        timeElapsed = '';

    if (timeElapsedHour) {
      timeElapsed += addZero(timeElapsedHour) + ':';
    }

    if (timeElapsedMinRem) {
      timeElapsed += addZero(timeElapsedMinRem) + ':';
    }

    timeElapsed += addZero(timeElapsedSecRem) + 's';

    return timeElapsed;
  };

  $scope.getDate = function (timestamp) {
    return moment.unix(parseInt(timestamp/1000)).format('MMMM Do');
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    location.hash = '#/goal-details/'+idUserGoal;
  };

  function addZero(digit) {
    if (digit < 10) {
      return '0'+digit;
    } else {
      return digit;
    }
  }

};