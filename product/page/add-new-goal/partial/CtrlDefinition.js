var CtrlDefinition = function ($scope, sharedDataAddNewGoal, pageManager) {
  pageManager.setTitle('Definition');

  if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
    location.hash = '#/add-new-goal/introduction';
    return;
  }

  $scope.model = {};
  $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
  $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;
};