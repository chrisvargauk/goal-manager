var CtrlListReward = function ($scope, sharedDataAddNewGoal, pageManager) {
  console.log('Rewards Controller is loaded.');

  var init = function () {
    pageManager.setTitle('Rewards');

    if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
      location.hash = '#/add-new-goal/introduction';
      return;
    }

    $scope.model = {};
    $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
    $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    location.hash = '#/goal-details/'+idUserGoal;
  };

  init();

};