var resolveReward = function () {
  return {
    srvClStep: function($q, $timeout, sharedDataAddNewGoal){
      var deferred = $q.defer();
      $timeout(function() {
//        var sharedDataAddNewGoal = {
//          lmGoal: {
//            id: 1
//          }
//        };

        sharedDataAddNewGoal.lmGoal = sharedDataAddNewGoal.lmGoal || {
          id: 1
        };

        var model = {};
        model.sharedDataAddNewGoal = sharedDataAddNewGoal;
//        model.lcStep = new LinkedCollection('step', $scope, {
        model.lcStep = new LinkedCollection('step', undefined, {
          idGoal: '',
          content: ''
        }, 'idGoal='+sharedDataAddNewGoal.lmGoal.id);

        pubsub.subscribe('collectionReady.step', function collectionReadyGoal() {
          deferred.resolve(model.lcStep);
          console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ sharedDataAddNewGoal', sharedDataAddNewGoal);
        });
      }, 0);

//      var sharedDataAddNewGoal = {
//        lmGoal: {
//          id: 1
//        }
//      };
//
//      sharedDataAddNewGoal.lmGoal = sharedDataAddNewGoal.lmGoal || {
//        id: 1
//      };
//
//      var model = {};
//      model.sharedDataAddNewGoal = sharedDataAddNewGoal;
//      model.lcStep = new LinkedCollection('step', $scope, {
//        idGoal: '',
//        content: ''
//      }, 'idGoal='+sharedDataAddNewGoal.lmGoal.id);
//
//      pubsub.subscribe('collectionReady.step', function collectionReadyGoal() {
//        deferred.resolve(model.lcStep);
//      });

      return deferred.promise;
    }
  }
};