var CtrlListTimeframe = function ($scope, cUserGoal, sharedDataAddNewGoal, pageManager) {
  var init = function () {
    pageManager.setTitle('Time Allocation');

    $scope.model = {};
    $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
    $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;
//    $scope.model.cUserGoal = cUserGoal;
//    $scope.model.mUserGoal = cUserGoal.getById(1);
    $scope.model.cTimeAndLocation = $scope.model.mUserGoal.cTimeAndLocation;

    if ($scope.model.cTimeAndLocation.JSON.length === 0) {
      $scope.addEmptyTimeAndLocation();
    }

  };

  $scope.removeTimeAndLocation = function (id) {
    $scope.model.cTimeAndLocation.removeById('timeAndLocation', id, function() {
      $scope.$apply(function () {});
    })
  };

  $scope.addEmptyTimeAndLocation = function () {
    $scope.model.cTimeAndLocation.add({
      day: 'Monday',
      fromHour: '9',
      fromMin: '0',
      toHour: '10',
      toMin: '0',
      location: ''
    }, function () {
      $scope.$apply(function () {});
    });
  };

  $scope.goToUserGoalDetail = function (idUserGoal) {
    location.hash = '#/goal-details/'+idUserGoal;
  };

  init();
};