var CtrlListStep = function ($scope, sharedDataAddNewGoal, pageManager) {
  var init = function () {
    pageManager.setTitle('Steps');

    if (typeof sharedDataAddNewGoal.mUserGoal === 'undefined') {
      location.hash = '#/add-new-goal/introduction';
      return;
    }

    $scope.model = {};
    $scope.model.cUserGoal = sharedDataAddNewGoal.cUserGoal;
    $scope.model.mUserGoal = sharedDataAddNewGoal.mUserGoal;

    if ($scope.model.mUserGoal.cStep.JSON.length === 0) {
      $scope.addEmptyStep();
    }
  };

  $scope.removeStep = function (id) {
    $scope.model.mUserGoal.cStep.removeById('step', id, function() {
      $scope.$apply(function () {});
    })
  };

  $scope.addEmptyStep = function () {
    $scope.model.mUserGoal.cStep.add({
      description: '',
      reward: ''
    }, function () {
      $scope.$apply(function () {});
    });
  };

  init();
};