define(['Collection'], function () {
  window.deferredFnReason = collectionFactory.getDeferredFn('cReason', {
    type: 'reason',
    default: {
      description: 'description of reason'
    }
  });

  window.deferredFnStep = collectionFactory.getDeferredFn('cStep', {
    type: 'step',
    default: {
      description: 'description of step',
      reward: 'related reward',
      isDone: false
    }
  });

  window.deferredFnTimeAndLocation = collectionFactory.getDeferredFn('cTimeAndLocation', {
    type: 'timeAndLocation',
    default: {
      day: 'none',
      fromHour: '0',
      fromMin: '0',
      toHour: '0',
      toMin: '0',
      location: 'none'
    }
  });

  window.deferredFnTrackRecord = collectionFactory.getDeferredFn('cTrackRecord', {
    type: 'trackRecord',
    default: {
      timeStart: 'timestamp',  // timestamp
      timeEnd: 'timestamp',     // timestamp
      weekOfYear: '-1',
      dayOfWeek: '-1'
    }
  });

  window.deferredFnUserGoal = collectionFactory.getDeferredFn('cUserGoal', {
    type: 'userGoal',
    default: {
      name: 'Name of the Goal',
      description: 'Short Description',
      tipVisible: true,
      cReason: 'collectionType(@)reason',
      cStep: 'collectionType(@)step',
      cTimeAndLocation: 'collectionType(@)timeAndLocation',
      cTrackRecord: 'collectionType(@)trackRecord',
      timestamp: '-1',
      cReview: 'collectionType(@)review'
    },
    debug: false
  });

  window.deferredFnReview = collectionFactory.getDeferredFn('cReview', {
    type: 'review',
    default: {
      scheduled: '',
      wentWrong: '',
      whatDifferent: '',
      isDone: false
    },
    debug: false
  });
});