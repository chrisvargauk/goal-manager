var CtrlIntroduction = function ($scope, sharedDataAddNewGoal, cUserGoal, pageManager) {
  pageManager.setTitle('Set New Goal');

  sharedDataAddNewGoal.cUserGoal = cUserGoal;

  $scope.goToNextPage = function () {
    location.hash = '#/add-new-goal/definition';
  };

  $scope.createNewGoal = function () {
    cUserGoal.add({
      name: '',
      description: '',
      tipVisible: true,
      cReason: [],
      cStep: [],
      cTimeAndLocation: [],
      cTrackRecord: [],
      timestamp: moment().format('x')
    }, function(model) {
      sharedDataAddNewGoal.mUserGoal = model;

      $scope.goToNextPage();
    });
  };
};