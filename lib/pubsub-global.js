console.log('loaded: js/lib/pubsub-global.js');

var pubsub = (function (){
  var list = [];

  var subscribe = function (evt, fn) {
    list.push({
      evt: evt,
      fn: fn
    });
  };

  var unSubscribe = function (evt, fn) {
    list = list.filter(function (item) {
      return item.evt !== evt && item.fn !== fn;
    });
  };

  var publish = function (evt, data) {
    console.warn('Pubsub Evt Fired: ' + evt);

    list.forEach(function (subscriber) {
      if (subscriber.evt === evt) {
        subscriber.fn(evt, data);
      }
    });
  };

  return {
    subscribe: subscribe,
    unSubscribe: unSubscribe,
    publish: publish
  };
}());